# Lee parte de una hoja de cálculo publicamente leible
# Basado en https://developers.google.com/sheets/api/quickstart/ruby

require 'dotenv'
require "google/apis/sheets_v4"
require "googleauth"
require "googleauth/stores/file_token_store"
require "fileutils"


# Este URI es para aplicaciones de escritorio --así debe crearse la credencial
# OAuth en Google Cloud Platform
OOB_URI = "urn:ietf:wg:oauth:2.0:oob".freeze

NOMBRE_APLICACION = "super-prueba-sheets-desde-ruby".freeze

# Estas credenciales se descargan desde Google Cloud Platform tras crear un
# proyecto con credenciales OAuth y poniendo permisos para usuarios Externos
# en aplicación de Prueba y especificando unos pocos usuarios que la pueden usar
RUTA_CREDENCIALES = "credenciales.json".freeze

# El archivo testigo.yaml se creará automáticamente al completar primer
# flujo de autorización y tendrá los testigos de acceso de usuario y de refresco.
RUTA_TESTIGO = "testigo.yaml".freeze

# Permiso para ver,editar,crear y eliminar todas las hojas (otra opción
# que da la API es solo para ver todas las hojas de cálculo). Otra que
# podría experimentarse es AUTH_DRIVE_FILE que es con los archivo
# usados solo con esta App, pero no se si da acceso al API de Sheets
SCOPE = Google::Apis::SheetsV4::AUTH_SPREADSHEETS

##
# Asegurar credenciales válidas, bien al restaurar de archivos
# de credenciales guardadas o iniciando autorización OAuth2.
# Si se requiere autorización, se abrirá navegador para aprobar
# la solicitud.
#
# @return [Google::Auth::UserRefreshCredentials] OAuth2 credentials
def autorizar
  id_cliente = Google::Auth::ClientId.from_file RUTA_CREDENCIALES
  almacen_testigo = Google::Auth::Stores::FileTokenStore.new file: RUTA_TESTIGO
  autorizador = Google::Auth::UserAuthorizer.new id_cliente, SCOPE, almacen_testigo
  id_usuario = "default"
  credenciales = autorizador.get_credentials id_usuario
  if credenciales.nil?
    url = autorizador.get_authorization_url base_url: OOB_URI
    puts "Abra el siguiente URL en el navegador para ingresar "\
         "el código resultante tras autorización:\n" + url
    codigo = gets
    credenciales = autorizador.get_and_store_credentials_from_code(
      user_id: id_usuario, code: codigo, base_url: OOB_URI
    )
  end
  credenciales
end

# Inicializa la API
servicio = Google::Apis::SheetsV4::SheetsService.new
servicio.client_options.application_name = NOMBRE_APLICACION
servicio.authorization = autorizar

Dotenv.load
id_hojadecalculo = ENV['IDHOJADECALCULO']
if id_hojadecalculo.nil?
  puts "Falta export IDHOJADECALCULO=id en .env"
  exit 1
end
rango = "A2:E"
respuesta = servicio.get_spreadsheet_values id_hojadecalculo, rango
puts "No se encontró información." if respuesta.values.empty?
respuesta.values.each do |row|
  # Presenta columnas A y E, que corresponden a índices 0 y 4.
  puts "#{row[0]}, #{row[4]}"
end
