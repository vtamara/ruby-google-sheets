# Ejemplos de uso del API de Google Sheets desde Ruby

Copie .env.plantilla en .env y ponga allí el ID de una hoja de
cálculo que se pueda leer y escribir publicamente

Antes de crear aplicaciones en ruby, como se explica en 
https://stackoverflow.com/a/37342555/3063083
la autenticación y autorización puede probarse inicialmente con:
https://developers.google.com/oauthplayground/?code=4/0AX4XfWgIlkP8vv64Z5w7y8lfHNUOIHPf7B7py_Ryp7kXi2OBMM8TVZNJfq59PBp8sa13Bw&scope=https://www.googleapis.com/auth/spreadsheets 


Aunque resulta más explicado probar desde la documentación de la API de 
Google Sheets:
https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets.values/update

Al usar API Key allí se genera: "API keys are not supported by this API"
de manera que es obligatorio el uso de OAuth --que fuerza al usuario final 
a tener cuenta en Gmail.

